﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomationModule.Models;

namespace TestAutomationModule.Services
{
    public class ElasticServiceAgent
    {
        private Globals globals;

        public async Task<ResultItem<GetApplicationFunctionTreeModelResult>> GetApplicationFunctionTreeModel(GetApplicationFunctionTreeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetApplicationFunctionTreeModelResult>>(() =>
           {
               var result = new ResultItem<GetApplicationFunctionTreeModelResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new GetApplicationFunctionTreeModelResult()
               };

               var searchConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig
                   }
               };

               var dbReaderConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the config!";
                   return result;
               }

               result.Result.ConfigItem = dbReaderConfig.Objects1.SingleOrDefault();

               if (result.Result.ConfigItem == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No config found!";
                   return result;
               }

               var searchPath = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.ConfigItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_TestAutomation_located_at_Path.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_TestAutomation_located_at_Path.ID_Class_Right
                   }
               };

               var dbReaderPath = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderPath.GetDataObjectRel(searchPath);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error whiule getting the related Path!";
                   return result;
               }

               result.Result.Path = dbReaderPath.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).SingleOrDefault();

               if (result.Result.Path == null)
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Path found!";
                   return result;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<ApplicationFunctionTreeElement>>> SaveAutomationItem(List<clsOntologyItem> automationItems)
        {
            var taskResult = await Task.Run<ResultItem<List<ApplicationFunctionTreeElement>>>(() =>
           {
               var result = new ResultItem<List<ApplicationFunctionTreeElement>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<ApplicationFunctionTreeElement>()
               };

               var dbWriter = new OntologyModDBConnector(globals);

               if (automationItems.Any())
               {
                   result.ResultState = dbWriter.SaveObjects(automationItems);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while saving the objects!";
                       return result;
                   }
                   var relationConfig = new clsRelationConfig(globals);

                   var saveObjectAtts = automationItems.Select(item => relationConfig.Rel_ObjectAttribute(item, Config.LocalData.AttributeType_AutomationId, item.Additional1)).ToList();

                   if (saveObjectAtts.Any())
                   {
                       result.ResultState = dbWriter.SaveObjAtt(saveObjectAtts);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           var delete = automationItems.Select(item => new clsOntologyItem
                           {
                               GUID = item.GUID
                           }).ToList();

                           if (delete.Any())
                           {
                               result.ResultState = dbWriter.DelObjects(delete);
                           }

                           result.ResultState.Additional1 = "Error while saving the attributes!";
                           return result;
                        }

                   }

                   result.Result = (from item in automationItems
                                    join att in saveObjectAtts on item.GUID equals att.ID_Object
                                    select new ApplicationFunctionTreeElement
                                    {
                                        ApplicationFunctionTreeItem = item,
                                        AutomationId = att
                                    }).ToList();
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<ApplicationFunctionTreeElement>>> GetItemsByAutomationIds(List<string> automationIds)
        {
            var taskResult = await Task.Run<ResultItem<List<ApplicationFunctionTreeElement>>>(() =>
            {
                var result = new ResultItem<List<ApplicationFunctionTreeElement>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<ApplicationFunctionTreeElement>()
                };

                var searchItems = automationIds.Select(automationId => new clsObjectAtt
                {
                    ID_AttributeType = Config.LocalData.AttributeType_AutomationId.GUID,
                    Val_String = automationId
                }).ToList();

                var dbReaderAutomationIds = new OntologyModDBConnector(globals);

                if (searchItems.Any())
                {
                    result.ResultState = dbReaderAutomationIds.GetDataObjectAtt(searchItems);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the attributes!";
                        return result;
                    }

                    result.Result = dbReaderAutomationIds.ObjAtts.Where(att => automationIds.Contains(att.Val_String)).Select(att => new ApplicationFunctionTreeElement
                    {
                        ApplicationFunctionTreeItem = new clsOntologyItem
                        {
                            GUID = att.ID_Object,
                            Name = att.Name_Object,
                            GUID_Parent = att.ID_Class,
                            Type = globals.Type_Object
                        },
                        AutomationId = att
                    }).ToList();
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ApplicationFunctionTreeElement>>> GetSubItems(ApplicationFunctionTreeElement parentItem)
        {
            var taskResult = await Task.Run<ResultItem<List<ApplicationFunctionTreeElement>>>(() =>
            {
                var result = new ResultItem<List<ApplicationFunctionTreeElement>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<ApplicationFunctionTreeElement>()
                };

                if (parentItem.ApplicationFunctionTreeItem.New_Item != null && parentItem.ApplicationFunctionTreeItem.New_Item.Value)
                {
                    return result;
                }

                var searchSubItems = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = parentItem.ApplicationFunctionTreeItem.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Application_Function_Tree_contains_Application_Function_Tree.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Application_Function_Tree_contains_Application_Function_Tree.ID_Class_Right
                    }
                };

                var dbReaderSubItems = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubItems.GetDataObjectRel(searchSubItems);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting related SubItems!";
                    return result;
                }

                result.Result.AddRange(dbReaderSubItems.ObjectRels.Select(rel => new ApplicationFunctionTreeElement
                {
                    ApplicationFunctionTreeItem = new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }
                }));

                var searchAutomationId = result.Result.Select(item => new clsObjectAtt
                {
                    ID_Object = item.ApplicationFunctionTreeItem.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Application_Function_Tree_AutomationId.ID_AttributeType
                }).ToList();

                var dbReaderAutomationId = new OntologyModDBConnector(globals);

                if (searchAutomationId.Any())
                {
                    result.ResultState = dbReaderAutomationId.GetDataObjectAtt(searchAutomationId);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the automationIds of the items!";
                        return result;
                    }

                    result.Result.ForEach(item =>
                    {
                        item.AutomationId = dbReaderAutomationId.ObjAtts.FirstOrDefault(att => att.ID_Object == item.ApplicationFunctionTreeItem.GUID);
                    });
                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<List<ApplicationFunctionTreeElement>>> SaveSubItemRelations(List<ApplicationFunctionTreeElement> subItems)
        {
            var taskResult = await Task.Run<ResultItem<List<ApplicationFunctionTreeElement>>>(() =>
            {
                var result = new ResultItem<List<ApplicationFunctionTreeElement>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = subItems
                };

                var dbWriter = new OntologyModDBConnector(globals);
                var relationConfig = new clsRelationConfig(globals);
                if (subItems.Any())
                {
                    var itemsToSave = subItems.Where(item => !string.IsNullOrEmpty(item.ApplicationFunctionTreeItem.Additional1) && item.ApplicationFunctionTreeItem.New_Item != null && item.ApplicationFunctionTreeItem.New_Item.Value).ToList();
                    var relations = subItems.Where(subItem => !string.IsNullOrEmpty(subItem.ApplicationFunctionTreeItem.Additional1)).Select(subItem => relationConfig.Rel_ObjectAttribute(subItem.ApplicationFunctionTreeItem, Config.LocalData.AttributeType_AutomationId, subItem.ApplicationFunctionTreeItem.Additional1)).ToList();

                    if (itemsToSave.Any())
                    {
                        result.ResultState = dbWriter.SaveObjects(itemsToSave.Select(item => item.ApplicationFunctionTreeItem).ToList());
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the Tree-items!";
                            return result;
                        }
                    }

                    

                    if (relations.Any())
                    {
                        result.ResultState = dbWriter.SaveObjAtt(relations);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while saving the AutomationIds!";
                            return result;
                        }
                    }

                    subItems.ForEach(subItem =>
                    {
                        subItem.AutomationId = relations.FirstOrDefault(rel => rel.Val_String == subItem.ApplicationFunctionTreeItem.Additional1);
                    });

                    long id = 1;
                    var parSubRel = itemsToSave.Where(item => item.ParentItem != null).Select(itemToSave => relationConfig.Rel_ObjectRelation(itemToSave.ParentItem.ApplicationFunctionTreeItem, itemToSave.ApplicationFunctionTreeItem, Config.LocalData.RelationType_contains, orderId: id++)).ToList();

                    result.ResultState = dbWriter.SaveObjRel(parSubRel);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relation between parent and sub!";
                        return result;
                    }


                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> DeleteSubItemRelations(clsOntologyItem parentItem, List<clsOntologyItem> subItems)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
           {
               var result = globals.LState_Success.Clone();

               if (subItems.Any())
               {
                   var delRel = subItems.Select(subItem => new clsObjectRel
                   {
                       ID_Object = parentItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Application_Function_Tree_contains_Application_Function_Tree.ID_RelationType,
                       ID_Other = subItem.GUID
                   }).ToList();

                   var dbWriter = new OntologyModDBConnector(globals);

                   result = dbWriter.DelObjectRels(delRel);

                   if (result.GUID == globals.LState_Error.GUID)
                   {
                       result.Additional1 = "Error while deleting the relation between the Parent- and the SubItems!";
                   }
               }

               return result;
           });

            return taskResult;
        }

        public ElasticServiceAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
