﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomationModule.Models
{
    public class GetApplicationFunctionTreeResult
    {
        public int CountFunctionTreeItems { get; set; }
    }
}
