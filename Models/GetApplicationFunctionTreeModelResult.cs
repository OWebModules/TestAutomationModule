﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomationModule.Models
{
    public class GetApplicationFunctionTreeModelResult
    {
        public clsOntologyItem ConfigItem { get; set; }
        public clsOntologyItem Path { get; set; }
    }
}
