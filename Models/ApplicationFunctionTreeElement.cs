﻿using FlaUI.Core.AutomationElements;
using FlaUI.Core.AutomationElements.Infrastructure;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomationModule.Models
{
    public class ApplicationFunctionTreeElement
    {
        public Window Window { get; set; }
        public AutomationElement AutomationElement { get; set; }
        public clsOntologyItem ApplicationFunctionTreeItem { get; set; }
        public clsObjectAtt AutomationId { get; set; }
        public List<ApplicationFunctionTreeElement> SubElements { get; set; } = new List<ApplicationFunctionTreeElement>();
        public ApplicationFunctionTreeElement ParentItem { get; set; }

        public List<ApplicationFunctionTreeElement> GetAllSubApplicationFunctionTreeElements()
        {
            var result = new List<ApplicationFunctionTreeElement>();
            result.AddRange(SubElements);
            SubElements.ForEach(subElement =>
            {
                result.AddRange(subElement.GetAllSubApplicationFunctionTreeElements());
            });

            return result;
        }
    }
}
