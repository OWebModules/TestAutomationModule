﻿using FlaUI.Core.AutomationElements.Infrastructure;
using FlaUI.Core.Conditions;
using FlaUI.UIA3;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using TestAutomationModule.Models;
using TestAutomationModule.Services;

namespace TestAutomationModule
{
    public class TestAutomationController : AppController
    {
        public async Task<ResultItem<GetApplicationFunctionTreeResult>> GetApplicationFunctionTree(GetApplicationFunctionTreeRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetApplicationFunctionTreeResult>>(async() =>
            {
                var result = new ResultItem<GetApplicationFunctionTreeResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetApplicationFunctionTreeResult()
                };

                var elasticAgent = new Services.ElasticServiceAgent(Globals);

                var serviceResult = await elasticAgent.GetApplicationFunctionTreeModel(request);

                result.ResultState = serviceResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                try
                {
                    Process process = null;
                    var wmiQueryString = "SELECT ProcessId, ExecutablePath, CommandLine FROM Win32_Process";
                    using (var searcher = new ManagementObjectSearcher(wmiQueryString))
                    using (var results = searcher.Get())
                    {
                        var query = from p in Process.GetProcesses()
                                    join mo in results.Cast<ManagementObject>()
                                    on p.Id equals (int)(uint)mo["ProcessId"]
                                    select new
                                    {
                                        Process = p,
                                        Path = (string)mo["ExecutablePath"],
                                        CommandLine = (string)mo["CommandLine"],
                                    };

                        process = query.FirstOrDefault(item => item.Path == serviceResult.Result.Path.Name)?.Process;
                    }

                    FlaUI.Core.Application app = null;
                    if (process != null)
                    {
                        app = FlaUI.Core.Application.Attach(process.Id);
                    }
                    else
                    {
                        app = FlaUI.Core.Application.Launch(serviceResult.Result.Path.Name);
                    }

                    using (var automation = new UIA3Automation())
                    {
                        var window = app.GetMainWindow(automation);

                        var applicationFunctionRootElement = new ApplicationFunctionTreeElement
                        {
                            ApplicationFunctionTreeItem = serviceResult.Result.ConfigItem
                        };

                        var subItemsResult = await elasticAgent.GetSubItems(applicationFunctionRootElement);

                        result.ResultState = subItemsResult.ResultState;

                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        if (!subItemsResult.Result.Any(subItem => subItem.AutomationId.Val_String == window.AutomationId))
                        {
                            applicationFunctionRootElement.SubElements.Add(new ApplicationFunctionTreeElement
                            {
                                ApplicationFunctionTreeItem = new clsOntologyItem
                                {
                                    GUID = Globals.NewGUID,
                                    Name = window.Name ?? window.AutomationId,
                                    GUID_Parent = Config.LocalData.Class_Application_Function_Tree.GUID,
                                    Additional1 = window.AutomationId,
                                    Type = Globals.Type_Object,
                                    New_Item = true
                                },
                                Window = window,
                                ParentItem = applicationFunctionRootElement
                            });
                        }
                        else if (subItemsResult.Result.Any() && !!subItemsResult.Result.Any(subItem => subItem.AutomationId.Val_String == window.AutomationId))
                        {
                            var delResult = await elasticAgent.DeleteSubItemRelations(applicationFunctionRootElement.ApplicationFunctionTreeItem, subItemsResult.Result.Select(item => item.ApplicationFunctionTreeItem).ToList());

                            result.ResultState = delResult;
                            if (result.ResultState.GUID == Globals.LState_Error.GUID)
                            {
                                return result;
                            }
                        }


                        if (applicationFunctionRootElement.SubElements.Any())
                        {
                            await GetSubElements(applicationFunctionRootElement.SubElements.First(), elasticAgent);
                        }

                        var subElements = applicationFunctionRootElement.GetAllSubApplicationFunctionTreeElements().Where(item => !string.IsNullOrEmpty(item.ApplicationFunctionTreeItem.Additional1)).ToList();

                        var saveResult = await elasticAgent.SaveSubItemRelations(subElements);
                    }
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Error while reading the Tree:{ex.Message}?";
                }
                
                return result;
            });

            return taskResult;
        }

        private async Task GetSubElements(ApplicationFunctionTreeElement parentElement, ElasticServiceAgent elasticAgent)
        {

            var subDbItems = await elasticAgent.GetSubItems(parentElement);

            var automationElementFirst = parentElement.AutomationElement ?? parentElement.Window;

            var children = new List<AutomationElement>();

            foreach (var child in automationElementFirst.FindAllChildren())
            {
                try
                {
                    if (child.ControlType != FlaUI.Core.Definitions.ControlType.DataItem &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.HeaderItem &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.ListItem &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.Separator &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.TabItem &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.ToolTip &&
                        child.ControlType != FlaUI.Core.Definitions.ControlType.TreeItem)
                    {
                        children.Add(child);
                    }
                }
                catch (Exception)
                {
                }
            }

            foreach (var childPre in children)
            {
                
                try
                {
                    var subDBItem = subDbItems.Result.FirstOrDefault(dbItem => dbItem.AutomationId.Val_String == childPre.AutomationId);

                    var automationElement = subDBItem != null ? subDBItem : new ApplicationFunctionTreeElement
                    {
                        AutomationElement = childPre,
                        ApplicationFunctionTreeItem = new clsOntologyItem
                        { 
                            GUID = Globals.NewGUID,
                            Name = !string.IsNullOrEmpty(childPre.Name) ? childPre.Name : childPre.AutomationId,
                            GUID_Parent = Config.LocalData.Class_Application_Function_Tree.GUID,
                            Type = Globals.Type_Object,
                            Additional1 = childPre.AutomationId,
                            New_Item = true
                        },
                        ParentItem = parentElement
                    };

                    parentElement.SubElements.Add(automationElement);
                }
                catch (Exception)
                {
                }
            }

            foreach (var subElement in parentElement.SubElements)
            {
                await GetSubElements(subElement, elasticAgent);
            }
        }


        public TestAutomationController(Globals globals) : base(globals)
        {
        }
    }
}
